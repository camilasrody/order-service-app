package Models;

public class Material {

    private int id;

    private String nome;
    private Float valorUnidade;
    private String tipoUnidade;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Float getValorUnidade() {
        return valorUnidade;
    }

    public void setValorUnidade(Float valorUnidade) {
        this.valorUnidade = valorUnidade;
    }

    public String getTipoUnidade() {
        return tipoUnidade;
    }

    public void setTipoUnidade(String tipoUnidade) {
        this.tipoUnidade = tipoUnidade;
    }

    @Override
    public String toString() {
        return "Material{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", valorUnidade='" + valorUnidade + '\'' +
                ", tipoUnidade='" + tipoUnidade + '\'' +
                '}';
    }

    public boolean isValid() {
        if (nome == null || nome.isEmpty()) {
            return false;
        }

        return true;
    }
}
