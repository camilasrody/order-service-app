<%@include file="../template/header.jsp" %>
<%@ page contentType="text/html;charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--${action == 'criar' ? '/tecnico?action=criar' : '/tecnico?action=editar&id=${tecnico.getId()}'}--%>

<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css"/>

<style>.bootstrap-iso .formden_header h2, .bootstrap-iso .formden_header p, .bootstrap-iso form {
    font-family: Arial, Helvetica, sans-serif;
    color: black
}

.bootstrap-iso form button, .bootstrap-iso form button:hover {
    color: white !important;
}

.asteriskField {
    color: red;
}</style>

<c:if test="${erro != null}">

    <p>${erro}</p>

</c:if>

<div class="bootstrap-iso">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <form method="post" action="<c:choose>
                                                <c:when test="${action == 'criar'}">
                                                    /tecnico?action=criar
                                                </c:when>
                                                <c:otherwise>
                                                    /tecnico?action=editar&id=${tecnico.getId()}
                                                </c:otherwise>
                                            </c:choose>">
                    <div class="form-group">
                        <label class="control-label requiredField" for="login">
                            Login
                            <span class="asteriskField"></span>
                        </label>
                        <input class="form-control" maxlength="30" id="login" name="login" value="${tecnico.getLogin()}"
                               placeholder="Login do Tecnico" type="text"/>
                    </div>

                    <div class="form-group">
                        <label class="control-label requiredField" for="senha">
                            Senha
                            <span class="asteriskField"></span>
                        </label>
                        <input class="form-control" maxlength="10" id="senha" name="senha"
                               value="${tecnico.getSenha()}"
                               placeholder="" type="password"/>
                    </div>

                    <div class="form-group">
                        <label class="control-label " for="admin">
                            Permiss&atilde;o
                        </label>
                        <select class="select form-control" id="admin" name="admin">
                            <option value="0" <c:if test="${tecnico.isLevel(0)}">selected</c:if>>
                                ${tecnico.getLevelName(0)}
                            </option>
                            <option value="1" <c:if test="${tecnico.isLevel(1)}">selected</c:if>>
                                ${tecnico.getLevelName(1)}
                            </option>
                            <option value="2" <c:if test="${tecnico.isLevel(2)}">selected</c:if>>
                                ${tecnico.getLevelName(2)}
                            </option>
                        </select>
                    </div>

                    <div class="form-group">
                        <div>
                            <button class="btn btn-primary " name="submit" type="submit">
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<%@include file="../template/footer.jsp" %>