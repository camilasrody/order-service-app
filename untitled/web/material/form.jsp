<%@include file="../template/header.jsp" %>
<%@ page contentType="text/html;charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--${action == 'criar' ? '/material?action=criar' : '/material?action=editar&id=${material.getId()}'}--%>

<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css"/>

<style>.bootstrap-iso .formden_header h2, .bootstrap-iso .formden_header p, .bootstrap-iso form {
    font-family: Arial, Helvetica, sans-serif;
    color: black
}

.bootstrap-iso form button, .bootstrap-iso form button:hover {
    color: white !important;
}

.asteriskField {
    color: red;
}</style>

<c:if test="${erro != null}">

    <p>${erro}</p>

</c:if>

<div class="bootstrap-iso">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <form method="post" action="<c:choose>
                                                <c:when test="${action == 'criar'}">
                                                    /material?action=criar
                                                </c:when>
                                                <c:otherwise>
                                                    /material?action=editar&id=${material.getId()}
                                                </c:otherwise>
                                            </c:choose>">
                    <div class="form-group">
                        <label class="control-label requiredField" for="nome">
                            Nome
                            <span class="asteriskField"></span>
                        </label>
                        <input class="form-control" maxlength="30" id="nome" name="nome" value="${material.getNome()}"
                               placeholder="Nome do Material" type="text"/>
                    </div>

                    <div class="form-group">
                        <label class="control-label requiredField" for="valorUnidade">
                            Valor Unidade
                            <span class="asteriskField"></span>
                        </label>
                        <input class="form-control" maxlength="10" id="valorUnidade" name="valorUnidade"
                               value="${material.getValorUnidade()}"
                               placeholder="xxx.xx" type="text"/>
                    </div>

                    <div class="form-group">
                        <label class="control-label requiredField" for="tipoUnidade">
                            Tipo Unidade
                            <span class="asteriskField"></span>
                        </label>
                        <input class="form-control" maxlength="10" id="tipoUnidade" name="tipoUnidade"
                               value="${material.getTipoUnidade()}"
                               placeholder="Unidade" type="text"/>
                    </div>

                    <div class="form-group">
                        <div>
                            <button class="btn btn-primary " name="submit" type="submit">
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<%@include file="../template/footer.jsp" %>