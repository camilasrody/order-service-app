<%@include file="../template/header.jsp" %>
<%@ page contentType="text/html;charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<header class="major">
    <h2>Lista de T�cnicos</h2>
</header>


<table border="1">
    <tr>
        <td>ID</td>
        <td>Login</td>
        <td>Senha</td>
        <td>Admin</td>
        <td>Comandos</td>
    </tr>
    <c:forEach var="tecnico" items="${lista}">
        <tr>
            <td>${tecnico.getId()}</td>
            <td>
                <a href="tecnico?action=ver&id=${tecnico.getId()}">${tecnico.getLogin()}</a>
            </td>
            <td>${tecnico.getSenha()}</td>
            <td>${tecnico.getLevelName()}</td>
            <td>
                <a href="tecnico?action=editar&id=${tecnico.getId()}">Editar</a>
                <a href="tecnico?action=deletar&id=${tecnico.getId()}">Excluir</a>
            </td>
        </tr>
    </c:forEach>
</table>

<c:if test="${fn:length(lista) > 0}">
    Existem ${fn:length(lista)} registros.
</c:if><br>

<a href="tecnico?action=criar">Adicionar</a>

<%@include file="../template/footer.jsp" %>