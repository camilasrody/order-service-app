<%@include file="../template/header.jsp" %>
<%@ page contentType="text/html;charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--${action == 'criar' ? '/Orcamento?action=criar' : '/Orcamento?action=editar&id=${orcamento.getId()}'}--%>

<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css"/>

<style>.bootstrap-iso .formden_header h2, .bootstrap-iso .formden_header p, .bootstrap-iso form {
    font-family: Arial, Helvetica, sans-serif;
    color: black
}

.bootstrap-iso form button, .bootstrap-iso form button:hover {
    color: white !important;
}

.asteriskField {
    color: red;
}</style>

<c:if test="${erro != null}">

    <p>${erro}</p>

</c:if>

<div class="bootstrap-iso">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <form method="post" action="<c:choose>
                                                <c:when test="${action == 'criar'}">
                                                    /orcamento?action=criar
                                                </c:when>
                                                <c:otherwise>
                                                    /orcamento?action=editar&id=${orcamento.getId()}
                                                </c:otherwise>
                                            </c:choose>">
                    <div class="form-group">
                        <label class="control-label requiredField" for="validade">
                            Data de Validade
                            <span class="asteriskField"></span>
                        </label>

                        <input class="form-control" id="validade" name="validade" value="${orcamento.formatValidade()}"
                               placeholder="xx/xx/xxxx" type="text" />
                    </div>

                    <div class="form-group">
                        <label class="control-label " for="cliente_id">
                            Cliente
                        </label>
                        <select class="select form-control" id="cliente_id" name="cliente_id">

                            <c:forEach var="cliente" items="${listaClientes}">

                                <option value="${cliente.getId()}" <c:if test="${orcamento.getCliente_id() == cliente.getId()}">selected</c:if>>
                                        ${cliente.getNome()}
                                </option>

                            </c:forEach>
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label " for="tecnico_id">
                            T�cnico
                        </label>
                        <select class="select form-control" id="tecnico_id" name="tecnico_id">

                            <c:forEach var="tecnico" items="${listaTecnicos}">

                                <option value="${tecnico.getId()}" <c:if test="${orcamento.getTecnico_id() == tecnico.getId()}">selected</c:if>>
                                        ${tecnico.getLogin()}
                                </option>

                            </c:forEach>
                        </select>
                    </div>

                    <div class="form-group">
                        <div>
                            <button class="btn btn-primary " name="submit" type="submit">
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<%@include file="../template/footer.jsp" %>