package DAO;

import Models.Cliente;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClienteDAO {

    public boolean incluir(Cliente cliente) {
        boolean resultado = false;

        Connection conexao = null;

        StringBuilder SQL = new StringBuilder();

        SQL.append("INSERT INTO Cliente(nome, email, tel1, tel2, rua, bairro, cidade, cep, complemento) ");
        SQL.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)");

        try {
            conexao = ConnectionFactory.createConnection();

            PreparedStatement ps = conexao.prepareStatement(SQL.toString());


            ps.setString(1, cliente.getNome());
            ps.setString(2, cliente.getEmail());
            ps.setString(3, cliente.getTel1());
            ps.setString(4, cliente.getTel2());
            ps.setString(5, cliente.getRua());
            ps.setString(6, cliente.getBairro());
            ps.setString(7, cliente.getCidade());
            ps.setString(8, cliente.getCep());
            ps.setString(9, cliente.getComplemento());

            int r = ps.executeUpdate();


            if (r > 0) {
                resultado = true;
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        return resultado;
    }

    public List<Cliente> listar() {
        Connection conexao = null;

        List<Cliente> lista = new ArrayList<Cliente>();

        StringBuilder SQL = new StringBuilder();

        SQL.append("SELECT * FROM Cliente");

        try {
            conexao = ConnectionFactory.createConnection();

            PreparedStatement ps = conexao.prepareStatement(SQL.toString());

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Cliente cliente = new Cliente();

                cliente.setId(rs.getInt("id"));
                cliente.setNome(rs.getString("nome"));
                cliente.setEmail(rs.getString("email"));
                cliente.setTel1(rs.getString("tel1"));
                cliente.setTel2(rs.getString("tel2"));
                cliente.setRua(rs.getString("rua"));
                cliente.setBairro(rs.getString("bairro"));
                cliente.setCidade(rs.getString("cidade"));
                cliente.setCep(rs.getString("cep"));
                cliente.setComplemento(rs.getString("complemento"));

                lista.add(cliente);
            }

            return lista;

        } catch (SQLException e) {
            e.printStackTrace();

            return null;
        }
    }

    public boolean deletar(int idCliente) {
        boolean resultado = false;

        Connection conexao = null;

        StringBuilder SQL = new StringBuilder();

        SQL.append("DELETE FROM Cliente WHERE id = ?;");

        try {
            conexao = ConnectionFactory.createConnection();

            PreparedStatement ps = conexao.prepareStatement(SQL.toString());

            ps.setInt(1, idCliente);


            int r = ps.executeUpdate();

            if (r > 0) {
                resultado = true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return resultado;
    }

    public Cliente obter(int idCliente) {

        Connection conexao = null;

        StringBuilder SQL = new StringBuilder();

        SQL.append("SELECT * FROM Cliente WHERE id = ?;");

        try {
            conexao = ConnectionFactory.createConnection();

            PreparedStatement ps = conexao.prepareStatement(SQL.toString());

            ps.setInt(1, idCliente);


            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Cliente cliente = new Cliente();

                cliente.setId(rs.getInt("id"));
                cliente.setNome(rs.getString("nome"));
                cliente.setEmail(rs.getString("email"));
                cliente.setTel1(rs.getString("tel1"));
                cliente.setTel2(rs.getString("tel2"));
                cliente.setRua(rs.getString("rua"));
                cliente.setBairro(rs.getString("bairro"));
                cliente.setCidade(rs.getString("cidade"));
                cliente.setCep(rs.getString("cep"));
                cliente.setComplemento(rs.getString("complemento"));

                return cliente;
            }

            return null;

        } catch (SQLException e) {
            e.printStackTrace();

            return null;
        }
    }

    public boolean editar(Cliente cliente) {
        boolean resultado = false;

        Connection conexao = null;

        StringBuilder SQL = new StringBuilder();

        SQL.append("UPDATE Cliente SET nome=?, email=?, tel1=?, tel2=?, rua=?, bairro=?, cidade=?, cep=?, complemento=? ");
        SQL.append("WHERE id=?");

        try {
            conexao = ConnectionFactory.createConnection();

            PreparedStatement ps = conexao.prepareStatement(SQL.toString());


            ps.setString(1, cliente.getNome());
            ps.setString(2, cliente.getEmail());
            ps.setString(3, cliente.getTel1());
            ps.setString(4, cliente.getTel2());
            ps.setString(5, cliente.getRua());
            ps.setString(6, cliente.getBairro());
            ps.setString(7, cliente.getCidade());
            ps.setString(8, cliente.getCep());
            ps.setString(9, cliente.getComplemento());
            ps.setInt(10, cliente.getId());

            if (ps.executeUpdate() > 0) {
                resultado = true;
            }

            ps.close();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        return resultado;
    }

    public List<Cliente> consultar(String nomeCliente) {
        boolean resultado = false;

        Connection conexao = null;

        List<Cliente> lista = new ArrayList<Cliente>();
        StringBuilder SQL = new StringBuilder();

        SQL.append("SELECT * FROM Cliente WHERE nomeCliente = ?;");

        try {
            conexao = ConnectionFactory.createConnection();

            PreparedStatement ps = conexao.prepareStatement(SQL.toString());

            ps.setString(1, nomeCliente);


            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Cliente cliente = new Cliente();

                cliente.setIdCliente(rs.getInt("idcliente"));
                cliente.setNomeCliente(rs.getString("nomeCliente"));
                cliente.setEnderecoCliente(rs.getString("enderecoCliente"));
                cliente.setBairroCliente(rs.getString("bairroCliente"));
                cliente.setCidadeCliente(rs.getString("cidadeCliente"));
                cliente.setEstadoCliente(rs.getString("estadoCliente"));
                cliente.setTelefoneCliente(rs.getString("telefoneCliente"));
                cliente.setEmailCliente(rs.getString("emailCliente"));

                lista.add(cliente);
            }

            return lista;

        } catch (SQLException e) {
            e.printStackTrace();

            return null;
        }
    }

}
