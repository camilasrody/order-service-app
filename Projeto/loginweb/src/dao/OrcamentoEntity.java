package dao;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by CamilaRody on 24/10/16.
 */
@Entity
@Table(name = "Orcamento", schema = "os", catalog = "")
public class OrcamentoEntity {
    private int id;
    private Timestamp validade;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "validade")
    public Timestamp getValidade() {
        return validade;
    }

    public void setValidade(Timestamp validade) {
        this.validade = validade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrcamentoEntity that = (OrcamentoEntity) o;

        if (id != that.id) return false;
        if (validade != null ? !validade.equals(that.validade) : that.validade != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (validade != null ? validade.hashCode() : 0);
        return result;
    }
}
