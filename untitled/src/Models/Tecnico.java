package Models;

public class Tecnico {

    private int id;

    private String login;
    private String senha;

    private int admin;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public int getAdmin() {
        return admin;
    }

    public void setAdmin(int admin) {
        this.admin = admin;
    }

    @Override
    public String toString() {
        return "Tecnico{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", senha='" + senha + '\'' +
                ", admin=" + admin +
                '}';
    }

    public boolean isValid() {
        if (login == null || login.isEmpty()) {
            return false;
        }
        if (senha == null || senha.isEmpty()) {
            return false;
        }

        return true;
    }

    /**
     * Retorna true se o dado tectico possui o nivel necessario fornecido pelo parametro
     *
     * @param levelNeeded
     * @return
     */
    public boolean isLevel(int levelNeeded) {
        return admin == levelNeeded;
    }

    public String getLevelName(){
        return this.getLevelName(this.getAdmin());
    }

    public String getLevelName(int level){

        String[] names = new String[3];
        names[0] = "T&eacute;cnico";
        names[1] = "T&eacute;cnico Admin";
        names[2] = "Super Admin";

        return names[level];

    }

}
