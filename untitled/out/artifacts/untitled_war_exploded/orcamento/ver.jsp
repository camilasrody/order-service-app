<%@include file="../template/header.jsp" %>
<%@ page contentType="text/html;charset=ISO-8859-1" import="java.util.Date, java.text.*" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${orcamento != null}">

    ${orcamento.getId()}<br/>
    Nome do cliente: ${orcamento.getCliente().getNome()}<br/>
    Nome do tecnico: ${orcamento.getTecnico().getLogin()}<br/>
    <fmt:formatDate value="${orcamento.validade}" type="both" pattern="dd/MM/yyyy"/>

</c:if>
<%@include file="../template/footer.jsp" %>