package DAO;

import Models.Orcamento;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrcamentoDAO {

    public boolean incluir(Orcamento orcamento) {
        boolean resultado = false;

        Connection conexao = null;

        StringBuilder SQL = new StringBuilder();

        SQL.append("INSERT INTO Orcamento(cliente_id, tecnico_id, validade) ");
        SQL.append("VALUES(?, ?, ?)");

        try {
            conexao = ConnectionFactory.createConnection();

            PreparedStatement ps = conexao.prepareStatement(SQL.toString());


            ps.setInt(1, orcamento.getCliente_id());
            ps.setInt(2, orcamento.getTecnico_id());
            ps.setDate(3, new java.sql.Date(orcamento.getValidade().getTime()));

            int r = ps.executeUpdate();


            if (r > 0) {
                resultado = true;
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        return resultado;
    }

    public List<Orcamento> listar() {
        Connection conexao = null;

        List<Orcamento> lista = new ArrayList<Orcamento>();

        StringBuilder SQL = new StringBuilder();

        SQL.append("SELECT * FROM Orcamento");

        try {
            conexao = ConnectionFactory.createConnection();

            PreparedStatement ps = conexao.prepareStatement(SQL.toString());

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Orcamento orcamento = new Orcamento();

                orcamento.setId(rs.getInt("id"));
                orcamento.setCliente_id(rs.getInt("cliente_id"));
                orcamento.setTecnico_id(rs.getInt("tecnico_id"));
                orcamento.setValidade(new java.util.Date(rs.getDate("validade").getTime()));

                lista.add(orcamento);
            }

            return lista;

        } catch (SQLException e) {
            e.printStackTrace();

            return null;
        }
    }

    public boolean deletar(int idOrcamento) {
        boolean resultado = false;

        Connection conexao = null;

        StringBuilder SQL = new StringBuilder();

        SQL.append("DELETE FROM Orcamento WHERE id = ?;");

        try {
            conexao = ConnectionFactory.createConnection();

            PreparedStatement ps = conexao.prepareStatement(SQL.toString());

            ps.setInt(1, idOrcamento);


            int r = ps.executeUpdate();

            if (r > 0) {
                resultado = true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return resultado;
    }

    public Orcamento obter(int idOrcamento) {

        Connection conexao = null;

        StringBuilder SQL = new StringBuilder();

        SQL.append("SELECT * FROM Orcamento WHERE id = ?;");

        try {
            conexao = ConnectionFactory.createConnection();

            PreparedStatement ps = conexao.prepareStatement(SQL.toString());

            ps.setInt(1, idOrcamento);


            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Orcamento orcamento = new Orcamento();

                orcamento.setId(rs.getInt("id"));
                orcamento.setCliente_id(rs.getInt("cliente_id"));
                orcamento.setTecnico_id(rs.getInt("tecnico_id"));
                orcamento.setValidade(new java.util.Date(rs.getDate("validade").getTime()));

                return orcamento;
            }

            return null;

        } catch (SQLException e) {
            e.printStackTrace();

            return null;
        }
    }

    public boolean editar(Orcamento orcamento) {
        boolean resultado = false;

        Connection conexao = null;

        StringBuilder SQL = new StringBuilder();

        SQL.append("UPDATE Orcamento SET cliente_id=?, tecnico_id=?, validade=? ");
        SQL.append("WHERE id=?");

        try {
            conexao = ConnectionFactory.createConnection();

            PreparedStatement ps = conexao.prepareStatement(SQL.toString());

            ps.setInt(1, orcamento.getCliente_id());
            ps.setInt(2, orcamento.getTecnico_id());
            ps.setDate(3, new java.sql.Date(orcamento.getValidade().getTime()));
            ps.setInt(4, orcamento.getId());

            if (ps.executeUpdate() > 0) {
                resultado = true;
            }

            ps.close();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        return resultado;
    }


}
