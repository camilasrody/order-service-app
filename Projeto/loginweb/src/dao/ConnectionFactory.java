package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
	public static Connection createConnection() throws SQLException{
		Connection conexao = null;
		
		String driverName = "com.mysql.jdbc.Driver";

		String url = "jdbc:mysql://127.0.0.1:3306/Login";
		String user = "root";
		String password = "";
		
		try {
			Class.forName(driverName);
			
			conexao = DriverManager.getConnection(url, user, password);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return conexao;
	}
}
