package dao;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by CamilaRody on 24/10/16.
 */
public class OrcamentoHasMaterialEntityPK implements Serializable {
    private int orcamentoId;
    private int materialId;

    @Column(name = "orcamento_id")
    @Id
    public int getOrcamentoId() {
        return orcamentoId;
    }

    public void setOrcamentoId(int orcamentoId) {
        this.orcamentoId = orcamentoId;
    }

    @Column(name = "material_id")
    @Id
    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrcamentoHasMaterialEntityPK that = (OrcamentoHasMaterialEntityPK) o;

        if (orcamentoId != that.orcamentoId) return false;
        if (materialId != that.materialId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = orcamentoId;
        result = 31 * result + materialId;
        return result;
    }
}
