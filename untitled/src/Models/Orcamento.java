package Models;

import DAO.ClienteDAO;
import DAO.TecnicoDAO;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Orcamento {

    private int id;

    private int cliente_id;
    private int tecnico_id;
    private Date validade;

    private Cliente cliente;
    private Tecnico tecnico;


    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    private void loadCliente(){
        ClienteDAO clienteDAO = new ClienteDAO();
        this.setCliente(clienteDAO.obter(cliente_id));
    }

    public Tecnico getTecnico() {
        return tecnico;
    }

    public void setTecnico(Tecnico tecnico) {
        this.tecnico = tecnico;
    }

    private void loadTecnico() {
        TecnicoDAO tecnicoDAO = new TecnicoDAO();
        this.setTecnico(tecnicoDAO.obter(tecnico_id));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(int cliente_id) {
        this.cliente_id = cliente_id;
        loadCliente();
    }

    public int getTecnico_id() {
        return tecnico_id;
    }
    public void setTecnico_id(int tecnico_id) {
        this.tecnico_id = tecnico_id;
        loadTecnico();
    }

    public Date getValidade() {
        return validade;
    }

    public String formatValidade(){
        if(null == this.validade){
            return "";
        }
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        return df.format(this.validade);
    }

    public void setValidade(Date validade) {
        this.validade = validade;
    }

    @Override
    public String toString() {
        return "Orcamento{" +
                "id=" + id +
                ", cliente_id=" + cliente_id +
                ", tecnico_id=" + tecnico_id +
                ", validade=" + validade +
                ", cliente=" + cliente +
                ", tecnico=" + tecnico +
                '}';
    }


    public boolean isValid() {
        if (validade == null) {
            return false;
        }

        return true;
    }

}
