package Controllers;

import DAO.ClienteDAO;
import Models.Cliente;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ClienteController implements ControllerContract {
    @Override
    public void listar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        ClienteDAO clienteDAO = new ClienteDAO();
        List<Cliente> lista = clienteDAO.listar();

        request.setAttribute("lista", lista);

        request.getRequestDispatcher("cliente/listar.jsp").forward(request, response);
    }

    @Override
    public void ver(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        ClienteDAO clienteDAO = new ClienteDAO();

        // Instanciar dados do banco
        Cliente cliente = clienteDAO.obter(Integer.parseInt(request.getParameter("id")));

        // Passa objeto para view
        request.setAttribute("cliente", cliente);

        request.getRequestDispatcher("cliente/ver.jsp").forward(request, response);
    }

    @Override
    public void deletar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        ClienteDAO clienteDAO = new ClienteDAO();
        if (clienteDAO.deletar(Integer.parseInt(request.getParameter("id")))) {
            response.sendRedirect("/cliente");
        } else {
            response.getWriter().write("<html><body>Erro ao deletar</body></html>");
        }
    }

    @Override
    public void criar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        editar(request, response);
    }

    @Override
    public void editar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String action = request.getParameter("action");

        // Flag de validação
        boolean valid = false;

        ClienteDAO clienteDAO = new ClienteDAO();

        // Instanciar objeto vazio
        Cliente cliente = new Cliente();


        if (action.equals("editar")) {
            // Instanciar dados do banco
            cliente = clienteDAO.obter(Integer.parseInt(request.getParameter("id")));
        }

        // Caso o formulario tenha cido preenchido,
        // setar o objeto com os dados recebidos
        if (request.getMethod().equals("POST")) {

            // Recebe campos do formulario
            cliente.setNome(request.getParameter("nome"));
            cliente.setEmail(request.getParameter("email"));
            cliente.setTel1(request.getParameter("tel1"));
            cliente.setTel2(request.getParameter("tel2"));
            cliente.setRua(request.getParameter("rua"));
            cliente.setBairro(request.getParameter("bairro"));
            cliente.setCidade(request.getParameter("cidade"));
            cliente.setCep(request.getParameter("cep"));
            cliente.setComplemento(request.getParameter("complemento"));

            // Validação de backend
            valid = cliente.isValid();
            if (valid) {

                if (action.equals("editar")) {
                    valid = clienteDAO.editar(cliente);
                } else {
                    valid = clienteDAO.incluir(cliente);
                }

            }

            if (valid) {
                response.sendRedirect("/cliente");
            } else {
                // Formulario enviado, objeto é valido, porem ocorreu um erro
                request.setAttribute("erro", "Ocorreu um erro ao salvar os dados");
            }

        }

        if (!valid) {
            // Passa objeto para view
            request.setAttribute("cliente", cliente);

            request.getRequestDispatcher("cliente/form.jsp").forward(request, response);
        }

    }

}
