package Controllers;

import DAO.MaterialDAO;
import Models.Material;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class MaterialController implements ControllerContract {
    @Override
    public void listar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        MaterialDAO materialDAO = new MaterialDAO();
        List<Material> lista = materialDAO.listar();

        request.setAttribute("lista", lista);

        request.getRequestDispatcher("material/listar.jsp").forward(request, response);
    }

    @Override
    public void ver(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        MaterialDAO materialDAO = new MaterialDAO();

        // Instanciar dados do banco
        Material material = materialDAO.obter(Integer.parseInt(request.getParameter("id")));

        // Passa objeto para view
        request.setAttribute("material", material);

        request.getRequestDispatcher("material/ver.jsp").forward(request, response);
    }

    @Override
    public void deletar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        MaterialDAO materialDAO = new MaterialDAO();
        if (materialDAO.deletar(Integer.parseInt(request.getParameter("id")))) {
            response.sendRedirect("/material");
        } else {
            response.getWriter().write("<html><body>Erro ao deletar</body></html>");
        }
    }

    @Override
    public void criar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        editar(request, response);
    }

    @Override
    public void editar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String action = request.getParameter("action");

        // Flag de validação
        boolean valid = false;

        MaterialDAO materialDAO = new MaterialDAO();

        // Instanciar objeto vazio
        Material material = new Material();


        if (action.equals("editar")) {
            // Instanciar dados do banco
            material = materialDAO.obter(Integer.parseInt(request.getParameter("id")));
        }

        // Caso o formulario tenha cido preenchido,
        // setar o objeto com os dados recebidos
        if (request.getMethod().equals("POST")) {

            // Recebe campos do formulario
            material.setNome(request.getParameter("nome"));
            material.setValorUnidade(Float.parseFloat(request.getParameter("valorUnidade")));
            material.setTipoUnidade(request.getParameter("tipoUnidade"));

            // Validação de backend
            valid = material.isValid();
            if (valid) {

                if (action.equals("editar")) {
                    valid = materialDAO.editar(material);
                } else {
                    valid = materialDAO.incluir(material);
                }

            }

            if (valid) {
                response.sendRedirect("/material");
            } else {
                // Formulario enviado, objeto é valido, porem ocorreu um erro
                request.setAttribute("erro", "Ocorreu um erro ao salvar os dados");
            }

        }

        if (!valid) {
            // Passa objeto para view
            request.setAttribute("material", material);

            request.getRequestDispatcher("material/form.jsp").forward(request, response);
        }

    }

}
