<%@include file="../template/header.jsp" %>
<%@ page contentType="text/html;charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<header class="major">
    <h2>Lista de Materiais</h2>
</header>


<table border="1">
    <tr>
        <td>ID</td>
        <td>Nome</td>
        <td>Valor Unidade</td>
        <td>Tipo Unidade</td>
        <td>Comandos</td>
    </tr>
    <c:forEach var="material" items="${lista}">
        <tr>
            <td>${material.getId()}</td>
            <td>
                <a href="material?action=ver&id=${material.getId()}">${material.getNome()}</a>
            </td>
            <td>${material.getValorUnidade()}</td>
            <td>${material.getTipoUnidade()}</td>
            <td>
                <a href="material?action=editar&id=${material.getId()}">Editar</a>
                <a href="material?action=deletar&id=${material.getId()}">Excluir</a>
            </td>
        </tr>
    </c:forEach>
</table>

<c:if test="${fn:length(lista) > 0}">
    Existem ${fn:length(lista)} registros.
</c:if><br>

<a href="material?action=criar">Adicionar</a>

<%@include file="../template/footer.jsp" %>