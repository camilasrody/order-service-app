package Controllers;

import DAO.TecnicoDAO;
import Models.Tecnico;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class BaseServlet extends HttpServlet {

    private String action = "listar";
    protected String resourceName = null;
    protected ControllerContract controller;

    public void init(ServletConfig servletConfig) throws ServletException {
        this.resourceName = servletConfig.getInitParameter("resourceName");

        if (this.resourceName.equals("material")) {
            this.controller = new MaterialController();
        } else if (this.resourceName.equals("tecnico")) {
            this.controller = new TecnicoController();
        } else if (this.resourceName.equals("cliente")) {
            this.controller = new ClienteController();
        } else if (this.resourceName.equals("orcamento")) {
            this.controller = new OrcamentoController();
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Define atributo do BaseServlet com base na URL
        if (request.getParameter("action") == null) {
            this.action = "listar";
        } else {
            this.action = request.getParameter("action");
        }

        // Passando parametro action para todos JSP
        request.setAttribute("action", request.getParameter("action"));

        // Passando tecnico logado para todos JSP
        request.setAttribute("loggedTecnico", this.getLoggedTecnico(request.getCookies()));

        // Comportamento de rota, incomum a todos servlets que fazem CRUD
        if (this.action.equals("listar")) {
            this.controller.listar(request, response);
        } else if (this.action.equals("ver")) {
            this.controller.ver(request, response);
        } else if (this.action.equals("deletar")) {
            this.controller.deletar(request, response);
        } else if (this.action.equals("criar")) {
            this.controller.criar(request, response);
        } else if (this.action.equals("editar")) {
            this.controller.editar(request, response);
        } else {
            response.getWriter().write("<html><body>Erro: action not defined: " + this.action + "</body></html>");
        }

    }

    /**
     * Recebe o array de cookies e retorna o Tecnico da sessão
     *
     * @param cookies
     * @return
     */
    private Tecnico getLoggedTecnico(Cookie[] cookies) {
        Tecnico loggedTecnico = null;

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("tecnico")) {
                    TecnicoDAO tecnicoDAO = new TecnicoDAO();
                    loggedTecnico = tecnicoDAO.obter(Integer.parseInt(cookie.getValue()));
                }
            }
        }

        return loggedTecnico;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doPost(request, response);

    }

}
