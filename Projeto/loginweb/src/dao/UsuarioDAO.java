package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import modelo.Usuario;

public class UsuarioDAO {
    public int incluir(Usuario u) {
        Connection c = null;

        String SQL = "INSERT INTO usuario(nomeUsuario, nomeCompletoUsuario, senha) VALUES(?, ?, ?)";

        int retorno = 0;

        try {
            c = ConnectionFactory.createConnection();

            PreparedStatement ps = c.prepareStatement(SQL);

            ps.setString(1, u.getNomeUsuario());
            ps.setString(2, u.getNomeCompletoUsuario());
            ps.setString(3, u.getSenha());

            retorno = ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return retorno;
    }

    public boolean verificar(String usuario, String senha) {
        Connection conexao = null;
        boolean retorno = false;

        StringBuilder SQL = new StringBuilder();

        SQL.append("SELECT * FROM usuario where nomeUsuario = ? AND senha = ?;");


        try {
            conexao = ConnectionFactory.createConnection();

            PreparedStatement ps = conexao.prepareStatement(SQL.toString());

            ps.setString(1, usuario);
            ps.setString(2, senha);

            ResultSet results = ps.executeQuery();

            if (results.next()) {
                retorno = true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return retorno;
    }

}

