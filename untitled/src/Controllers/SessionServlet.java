package Controllers;

import DAO.TecnicoDAO;
import Models.Tecnico;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/SessionServlet")
public class SessionServlet extends HttpServlet {

    public SessionServlet() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String login = request.getParameter("login");
        String senha = request.getParameter("senha");

        Tecnico tecnico = new Tecnico();
        tecnico.setLogin(login);
        tecnico.setSenha(senha);

        TecnicoDAO tecnicoDAO = new TecnicoDAO();
        tecnico = tecnicoDAO.login(tecnico);

        if (tecnico != null) {
            Cookie loginCookie = new Cookie("tecnico", Integer.toString(tecnico.getId()));
            // setting cookie to expiry in 30 mins
            loginCookie.setMaxAge(30 * 60);
            response.addCookie(loginCookie); // manda para o usuario

            // Redirect
            response.sendRedirect("cliente");
        } else {
            request.setAttribute("erro", "Dados de login inválidos.");

            // Load View
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        response.sendRedirect("/");
    }
}