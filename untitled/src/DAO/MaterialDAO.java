package DAO;

import Models.Material;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MaterialDAO {

    public boolean incluir(Material material) {
        boolean resultado = false;

        Connection conexao = null;

        StringBuilder SQL = new StringBuilder();

        SQL.append("INSERT INTO Material(nome, valorUnidade, tipoUnidade) ");
        SQL.append("VALUES(?, ?, ?)");

        try {
            conexao = ConnectionFactory.createConnection();

            PreparedStatement ps = conexao.prepareStatement(SQL.toString());


            ps.setString(1, material.getNome());
            ps.setFloat(2, material.getValorUnidade());
            ps.setString(3, material.getTipoUnidade());

            int r = ps.executeUpdate();


            if (r > 0) {
                resultado = true;
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        return resultado;
    }

    public List<Material> listar() {
        Connection conexao = null;

        List<Material> lista = new ArrayList<Material>();

        StringBuilder SQL = new StringBuilder();

        SQL.append("SELECT * FROM Material");

        try {
            conexao = ConnectionFactory.createConnection();

            PreparedStatement ps = conexao.prepareStatement(SQL.toString());

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Material material = new Material();

                material.setId(rs.getInt("id"));
                material.setNome(rs.getString("nome"));
                material.setValorUnidade(Float.parseFloat(rs.getString("valorUnidade")));
                material.setTipoUnidade(rs.getString("tipoUnidade"));

                lista.add(material);
            }

            return lista;

        } catch (SQLException e) {
            e.printStackTrace();

            return null;
        }
    }

    public boolean deletar(int idMaterial) {
        boolean resultado = false;

        Connection conexao = null;

        StringBuilder SQL = new StringBuilder();

        SQL.append("DELETE FROM Material WHERE id = ?;");

        try {
            conexao = ConnectionFactory.createConnection();

            PreparedStatement ps = conexao.prepareStatement(SQL.toString());

            ps.setInt(1, idMaterial);


            int r = ps.executeUpdate();

            if (r > 0) {
                resultado = true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return resultado;
    }

    public Material obter(int idMaterial) {

        Connection conexao = null;

        StringBuilder SQL = new StringBuilder();

        SQL.append("SELECT * FROM Material WHERE id = ?;");

        try {
            conexao = ConnectionFactory.createConnection();

            PreparedStatement ps = conexao.prepareStatement(SQL.toString());

            ps.setInt(1, idMaterial);


            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Material material = new Material();

                material.setId(rs.getInt("id"));
                material.setNome(rs.getString("nome"));
                material.setValorUnidade(Float.parseFloat(rs.getString("valorUnidade")));
                material.setTipoUnidade(rs.getString("tipoUnidade"));

                return material;
            }

            return null;

        } catch (SQLException e) {
            e.printStackTrace();

            return null;
        }
    }

    public boolean editar(Material material) {
        boolean resultado = false;

        Connection conexao = null;

        StringBuilder SQL = new StringBuilder();

        SQL.append("UPDATE Material SET nome=?, valorUnidade=?, tipoUnidade=? ");
        SQL.append("WHERE id=?");

        try {
            conexao = ConnectionFactory.createConnection();

            PreparedStatement ps = conexao.prepareStatement(SQL.toString());

            ps.setString(1, material.getNome());
            ps.setFloat(2, material.getValorUnidade());
            ps.setString(3, material.getTipoUnidade());
            ps.setInt(4, material.getId());

            if (ps.executeUpdate() > 0) {
                resultado = true;
            }

            ps.close();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        return resultado;
    }

//    public List<Material> consultar(String nomeMaterial) {
//        boolean resultado = false;
//
//        Connection conexao = null;
//
//        List<Material> lista = new ArrayList<Material>();
//        StringBuilder SQL = new StringBuilder();
//
//        SQL.append("SELECT * FROM Material WHERE nomeMaterial = ?;");
//
//        try {
//            conexao = ConnectionFactory.createConnection();
//
//            PreparedStatement ps = conexao.prepareStatement(SQL.toString());
//
//            ps.setString(1, nomeMaterial);
//
//
//            ResultSet rs = ps.executeQuery();
//
//            while (rs.next()) {
//                Material material = new Material();
//
//                material.setIdMaterial(rs.getInt("idmaterial"));
//                material.setNomeMaterial(rs.getString("nomeMaterial"));
//                material.setEnderecoMaterial(rs.getString("enderecoMaterial"));
//                material.setBairroMaterial(rs.getString("bairroMaterial"));
//                material.setCidadeMaterial(rs.getString("cidadeMaterial"));
//                material.setEstadoMaterial(rs.getString("estadoMaterial"));
//                material.setTelefoneMaterial(rs.getString("telefoneMaterial"));
//                material.setEmailMaterial(rs.getString("emailMaterial"));
//
//                lista.add(material);
//            }
//
//            return lista;
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//
//            return null;
//        }
//    }

}
