package Controllers;

import DAO.ClienteDAO;
import DAO.OrcamentoDAO;
import DAO.TecnicoDAO;
import Models.Cliente;
import Models.Orcamento;
import Models.Tecnico;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class OrcamentoController implements ControllerContract {
x
    @Override
    public void listar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        OrcamentoDAO orcamentoDAO = new OrcamentoDAO();
        List<Orcamento> lista = orcamentoDAO.listar();

        request.setAttribute("lista", lista);

        request.getRequestDispatcher("orcamento/listar.jsp").forward(request, response);
    }

    @Override
    public void ver(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        OrcamentoDAO orcamentoDAO = new OrcamentoDAO();

        // Instanciar dados do banco
        Orcamento orcamento = orcamentoDAO.obter(Integer.parseInt(request.getParameter("id")));

        // Passa objeto para view
        request.setAttribute("orcamento", orcamento);

        request.getRequestDispatcher("orcamento/ver.jsp").forward(request, response);
    }

    @Override
    public void deletar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        OrcamentoDAO orcamentoDAO = new OrcamentoDAO();
        if (orcamentoDAO.deletar(Integer.parseInt(request.getParameter("id")))) {
            response.sendRedirect("/orcamento");
        } else {
            response.getWriter().write("<html><body>Erro ao deletar</body></html>");
        }
    }

    @Override
    public void criar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        editar(request, response);
    }

    @Override
    public void editar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String action = request.getParameter("action");

        // Flag de validação
        boolean valid = false;

        OrcamentoDAO orcamentoDAO = new OrcamentoDAO();

        // Instanciar objeto vazio
        Orcamento orcamento = new Orcamento();

        // Obtem lista de Clientes
        ClienteDAO clienteDAO = new ClienteDAO();
        List<Cliente> listaClientes = clienteDAO.listar();
        request.setAttribute("listaClientes", listaClientes);

        // Obtem lista de Tecnicos
        TecnicoDAO TecnicoDAO = new TecnicoDAO();
        List<Tecnico> listaTecnicos = TecnicoDAO.listar();
        request.setAttribute("listaTecnicos", listaTecnicos);


        if (action.equals("editar")) {
            // Instanciar dados do banco
            orcamento = orcamentoDAO.obter(Integer.parseInt(request.getParameter("id")));
        }

        // Caso o formulario tenha cido preenchido,
        // setar o objeto com os dados recebidos
        if (request.getMethod().equals("POST")) {

            // Recebe campos do formulario
            orcamento.setCliente_id(Integer.parseInt(request.getParameter("cliente_id")));
            orcamento.setTecnico_id(Integer.parseInt(request.getParameter("tecnico_id")));

            // Faz a leitura da data de validade. Caso ocorra um erro de formatação
            // o sistema utilizará a data atual
            try {
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                orcamento.setValidade(df.parse(request.getParameter("validade")));
            } catch (Exception e) {
                orcamento.setValidade(new Date());
            }

            // Validação de backend
            valid = orcamento.isValid();
            if (valid) {

                if (action.equals("editar")) {
                    valid = orcamentoDAO.editar(orcamento);
                } else {
                    valid = orcamentoDAO.incluir(orcamento);
                }

            }

            if (valid) {
                response.sendRedirect("/orcamento");
            } else {
                // Formulario enviado, objeto é valido, porem ocorreu um erro
                request.setAttribute("erro", "Ocorreu um erro ao salvar os dados");
            }

        }

        if (!valid) {
            // Passa objeto para view
            request.setAttribute("orcamento", orcamento);

            request.getRequestDispatcher("orcamento/form.jsp").forward(request, response);
        }

    }

}
