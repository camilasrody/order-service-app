package Controllers;

import DAO.TecnicoDAO;
import Models.Tecnico;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class TecnicoController implements ControllerContract {

    @Override
    public void listar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        TecnicoDAO tecnicoDAO = new TecnicoDAO();
        List<Tecnico> lista = tecnicoDAO.listar();

        request.setAttribute("lista", lista);

        request.getRequestDispatcher("tecnico/listar.jsp").forward(request, response);
    }

    @Override
    public void ver(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        TecnicoDAO tecnicoDAO = new TecnicoDAO();

        // Instanciar dados do banco
        Tecnico tecnico = tecnicoDAO.obter(Integer.parseInt(request.getParameter("id")));

        // Passa objeto para view
        request.setAttribute("tecnico", tecnico);

        request.getRequestDispatcher("tecnico/ver.jsp").forward(request, response);
    }

    @Override
    public void deletar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        TecnicoDAO tecnicoDAO = new TecnicoDAO();
        if (tecnicoDAO.deletar(Integer.parseInt(request.getParameter("id")))) {
            response.sendRedirect("/tecnico");
        } else {
            response.getWriter().write("<html><body>Erro ao deletar</body></html>");
        }
    }

    @Override
    public void criar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        editar(request, response);
    }

    @Override
    public void editar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String action = request.getParameter("action");

        // Flag de validação
        boolean valid = false;

        TecnicoDAO tecnicoDAO = new TecnicoDAO();

        // Instanciar objeto vazio
        Tecnico tecnico = new Tecnico();


        if (action.equals("editar")) {
            // Instanciar dados do banco
            tecnico = tecnicoDAO.obter(Integer.parseInt(request.getParameter("id")));
        }

        // Caso o formulario tenha cido preenchido,
        // setar o objeto com os dados recebidos
        if (request.getMethod().equals("POST")) {

            // Recebe campos do formulario
            tecnico.setLogin(request.getParameter("login"));
            tecnico.setSenha(request.getParameter("senha"));
            tecnico.setAdmin(Integer.parseInt(request.getParameter("admin")));

            // Validação de backend
            valid = tecnico.isValid();
            if (valid) {

                if (action.equals("editar")) {
                    valid = tecnicoDAO.editar(tecnico);
                } else {
                    valid = tecnicoDAO.incluir(tecnico);
                }

            }

            if (valid) {
                response.sendRedirect("/tecnico");
            } else {
                // Formulario enviado, objeto é valido, porem ocorreu um erro
                request.setAttribute("erro", "Ocorreu um erro ao salvar os dados");
            }

        }

        if (!valid) {
            // Passa objeto para view
            request.setAttribute("tecnico", tecnico);

            request.getRequestDispatcher("tecnico/form.jsp").forward(request, response);
        }

    }

}
