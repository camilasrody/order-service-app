package dao;

import javax.persistence.*;

/**
 * Created by CamilaRody on 24/10/16.
 */
@Entity
@Table(name = "Orcamento_has_Material", schema = "os", catalog = "")
@IdClass(OrcamentoHasMaterialEntityPK.class)
public class OrcamentoHasMaterialEntity {
    private int orcamentoId;
    private int materialId;

    @Id
    @Column(name = "orcamento_id")
    public int getOrcamentoId() {
        return orcamentoId;
    }

    public void setOrcamentoId(int orcamentoId) {
        this.orcamentoId = orcamentoId;
    }

    @Id
    @Column(name = "material_id")
    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrcamentoHasMaterialEntity that = (OrcamentoHasMaterialEntity) o;

        if (orcamentoId != that.orcamentoId) return false;
        if (materialId != that.materialId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = orcamentoId;
        result = 31 * result + materialId;
        return result;
    }
}
