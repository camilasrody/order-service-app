package DAO;

import Models.Tecnico;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TecnicoDAO {

    public boolean incluir(Tecnico tecnico) {
        boolean resultado = false;

        Connection conexao = null;

        StringBuilder SQL = new StringBuilder();

        SQL.append("INSERT INTO Tecnico(login, senha, admin) ");
        SQL.append("VALUES(?, ?, ?)");

        try {
            conexao = ConnectionFactory.createConnection();

            PreparedStatement ps = conexao.prepareStatement(SQL.toString());


            ps.setString(1, tecnico.getLogin());
            ps.setString(2, tecnico.getSenha());
            ps.setInt(3, tecnico.getAdmin());

            int r = ps.executeUpdate();


            if (r > 0) {
                resultado = true;
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        return resultado;
    }

    public List<Tecnico> listar() {
        Connection conexao = null;

        List<Tecnico> lista = new ArrayList<Tecnico>();

        StringBuilder SQL = new StringBuilder();

        SQL.append("SELECT * FROM Tecnico");

        try {
            conexao = ConnectionFactory.createConnection();

            PreparedStatement ps = conexao.prepareStatement(SQL.toString());

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Tecnico tecnico = new Tecnico();

                tecnico.setId(rs.getInt("id"));
                tecnico.setLogin(rs.getString("login"));
                tecnico.setSenha(rs.getString("senha"));
                tecnico.setAdmin(Integer.parseInt(rs.getString("admin")));

                lista.add(tecnico);
            }

            return lista;

        } catch (SQLException e) {
            e.printStackTrace();

            return null;
        }
    }

    public boolean deletar(int idTecnico) {
        boolean resultado = false;

        Connection conexao = null;

        StringBuilder SQL = new StringBuilder();

        SQL.append("DELETE FROM Tecnico WHERE id = ? AND admin <> 2;");

        try {
            conexao = ConnectionFactory.createConnection();

            PreparedStatement ps = conexao.prepareStatement(SQL.toString());

            ps.setInt(1, idTecnico);


            int r = ps.executeUpdate();

            if (r > 0) {
                resultado = true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return resultado;
    }

    public Tecnico obter(int idTecnico) {

        Connection conexao = null;

        StringBuilder SQL = new StringBuilder();

        SQL.append("SELECT * FROM Tecnico WHERE id = ?;");

        try {
            conexao = ConnectionFactory.createConnection();

            PreparedStatement ps = conexao.prepareStatement(SQL.toString());

            ps.setInt(1, idTecnico);


            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Tecnico tecnico = new Tecnico();

                tecnico.setId(rs.getInt("id"));
                tecnico.setLogin(rs.getString("login"));
                tecnico.setSenha(rs.getString("senha"));
                tecnico.setAdmin(Integer.parseInt(rs.getString("admin")));

                return tecnico;
            }

            return null;

        } catch (SQLException e) {
            e.printStackTrace();

            return null;
        }
    }

    public boolean editar(Tecnico tecnico) {
        boolean resultado = false;

        Connection conexao = null;

        StringBuilder SQL = new StringBuilder();

        SQL.append("UPDATE Tecnico SET login=?, senha=?, admin=? ");
        SQL.append("WHERE id=?");

        try {
            conexao = ConnectionFactory.createConnection();

            PreparedStatement ps = conexao.prepareStatement(SQL.toString());

            ps.setString(1, tecnico.getLogin());
            ps.setString(2, tecnico.getSenha());
            ps.setInt(3, tecnico.getAdmin());
            ps.setInt(4, tecnico.getId());

            if (ps.executeUpdate() > 0) {
                resultado = true;
            }

            ps.close();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        return resultado;
    }

    public Tecnico login(Tecnico tecnico) {

        Connection conexao = null;

        StringBuilder SQL = new StringBuilder();

        SQL.append("SELECT * FROM Tecnico WHERE login = ? AND senha = ?;");

        try {
            conexao = ConnectionFactory.createConnection();

            PreparedStatement ps = conexao.prepareStatement(SQL.toString());

            ps.setString(1, tecnico.getLogin());
            ps.setString(2, tecnico.getSenha());

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                tecnico.setId(rs.getInt("id"));
                tecnico.setLogin(rs.getString("login"));
                tecnico.setSenha(rs.getString("senha"));
                tecnico.setAdmin(Integer.parseInt(rs.getString("admin")));

                return tecnico;
            }

            return null;

        } catch (SQLException e) {
            e.printStackTrace();

            return null;
        }

    }


//    public List<Tecnico> consultar(String loginTecnico) {
//        boolean resultado = false;
//
//        Connection conexao = null;
//
//        List<Tecnico> lista = new ArrayList<Tecnico>();
//        StringBuilder SQL = new StringBuilder();
//
//        SQL.append("SELECT * FROM Tecnico WHERE loginTecnico = ?;");
//
//        try {
//            conexao = ConnectionFactory.createConnection();
//
//            PreparedStatement ps = conexao.prepareStatement(SQL.toString());
//
//            ps.setString(1, loginTecnico);
//
//
//            ResultSet rs = ps.executeQuery();
//
//            while (rs.next()) {
//                Tecnico tecnico = new Tecnico();
//
//                tecnico.setIdTecnico(rs.getInt("idtecnico"));
//                tecnico.setLoginTecnico(rs.getString("loginTecnico"));
//                tecnico.setEnderecoTecnico(rs.getString("enderecoTecnico"));
//                tecnico.setBairroTecnico(rs.getString("bairroTecnico"));
//                tecnico.setCidadeTecnico(rs.getString("cidadeTecnico"));
//                tecnico.setEstadoTecnico(rs.getString("estadoTecnico"));
//                tecnico.setTelefoneTecnico(rs.getString("telefoneTecnico"));
//                tecnico.setEmailTecnico(rs.getString("emailTecnico"));
//
//                lista.add(tecnico);
//            }
//
//            return lista;
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//
//            return null;
//        }
//    }

}
