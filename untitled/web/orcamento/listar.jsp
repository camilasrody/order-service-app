<%@include file="../template/header.jsp" %>
<%@ page contentType="text/html;charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<header class="major">
    <h2>Lista de Or�amentos</h2>
</header>


<table border="1">
    <tr>
        <td>ID</td>
        <td>Cliente</td>
        <td>T�cnico</td>
        <td>Validade</td>
        <td>Comandos</td>
    </tr>
    <c:forEach var="Orcamento" items="${lista}">
        <tr>
            <td>
                <a href="orcamento?action=ver&id=${Orcamento.getId()}">${Orcamento.getId()}</a>
            </td>
            <td>
                <a href="cliente?action=ver&id=${Orcamento.getCliente_id()}">${Orcamento.getCliente().getNome()}</a>
            </td>
            <td>
                <a href="tecnico?action=ver&id=${Orcamento.getTecnico_id()}">${Orcamento.getTecnico().getLogin()}</a>
            </td>
            <td>
                <fmt:formatDate value="${Orcamento.validade}" type="both" pattern="dd/MM/yyyy"/>
            </td>
            <td>
                <a href="orcamento?action=editar&id=${Orcamento.getId()}">Editar</a>
                <a href="orcamento?action=deletar&id=${Orcamento.getId()}">Excluir</a>
            </td>
        </tr>
    </c:forEach>
</table>

<c:if test="${fn:length(lista) > 0}">
    Existem ${fn:length(lista)} registros.
</c:if><br>

<a href="orcamento?action=criar">Adicionar</a>

<%@include file="../template/footer.jsp" %>