<%@ page contentType="text/html;charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>Sistema de Ordem de Servi�os</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <script src="js/jquery.min.js"></script>

    <script src="js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <link rel="stylesheet" href="css/main.css" />
</head>
<body>

<!-- Wrapper -->
<div id="wrapper">

    <!-- Header -->
    <header id="header" class="alt">
        <span class="logo"><img src="images/logo.svg" alt="" /></span>
        <h1>Sistema de Controle de Servi�os</h1>
        <p>Projeto Final Senac 2016<br />
            Ol� ${loggedTecnico.getLogin()}!
        </p>
    </header>

    <!-- Nav -->
    <nav id="nav">
        <ul>
            <li><a href="cliente" class="active">Cliente</a></li>
            <li><a href="tecnico">T�cnico</a></li>
            <li><a href="orcamento">Or�amento</a></li>
            <li><a href="material">Material</a></li>
            <li><a href="SessionServlet">Logout</a></li>
        </ul>
    </nav>

    <!-- Main -->
    <div id="main">

        <!-- First Section -->
        <section class="main special">