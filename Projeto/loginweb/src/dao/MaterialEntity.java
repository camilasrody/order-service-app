package dao;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by CamilaRody on 24/10/16.
 */
@Entity
@Table(name = "Material", schema = "os", catalog = "")
public class MaterialEntity {
    private int id;
    private String nome;
    private BigDecimal valorUnidade;
    private String tipoUnidade;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nome")
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Basic
    @Column(name = "valorUnidade")
    public BigDecimal getValorUnidade() {
        return valorUnidade;
    }

    public void setValorUnidade(BigDecimal valorUnidade) {
        this.valorUnidade = valorUnidade;
    }

    @Basic
    @Column(name = "tipoUnidade")
    public String getTipoUnidade() {
        return tipoUnidade;
    }

    public void setTipoUnidade(String tipoUnidade) {
        this.tipoUnidade = tipoUnidade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MaterialEntity that = (MaterialEntity) o;

        if (id != that.id) return false;
        if (nome != null ? !nome.equals(that.nome) : that.nome != null) return false;
        if (valorUnidade != null ? !valorUnidade.equals(that.valorUnidade) : that.valorUnidade != null) return false;
        if (tipoUnidade != null ? !tipoUnidade.equals(that.tipoUnidade) : that.tipoUnidade != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (nome != null ? nome.hashCode() : 0);
        result = 31 * result + (valorUnidade != null ? valorUnidade.hashCode() : 0);
        result = 31 * result + (tipoUnidade != null ? tipoUnidade.hashCode() : 0);
        return result;
    }
}
