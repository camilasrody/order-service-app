<%@ page contentType="text/html;charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>Sistema de Ordem de Servi�os</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <script src="js/jquery.min.js"></script>

    <script src="js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <link rel="stylesheet" href="css/main.css"/>
</head>
<body>

<!-- Wrapper -->
<div id="wrapper">

    <!-- Header -->
    <header id="header" class="alt">
        <span class="logo"><img src="images/logo.svg" alt=""/></span>
        <h1>Sistema de Ordem de Servi�os</h1>
        <p>Projeto Final Senac 2016<br/>
            Ol� ${loggedTecnico.getLogin()}!
        </p>
    </header>


    <!-- Main -->
    <div id="main">

        <!-- First Section -->
        <section class="main special">
            <header class="major">
                <h2>�rea Restrita</h2>
            </header>

            <c:if test="${erro != null}">

                <p>${erro}</p>

            </c:if>

            <form action="SessionServlet" method="post">

                Login: <input type="text" name="login" value="${tecnico.getLogin()}"/>
                <br/>

                Senha: <input type="password" name="senha"/>
                <br/>

                <input type="submit" value="Login"/>

            </form>

        </section>

    </div>

    <!-- Footer -->
    <footer id="footer">
        <p class="copyright">&copy; Copyright 2016 SENAC Trabalho final - Developer by Camila Silva Rody</a>.</p>
    </footer>

</div>

</body>
</html>