<%@include file="../template/header.jsp" %>
<%@ page contentType="text/html;charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<header class="major">
    <h2>Lista de Clientes</h2>
</header>


<table border="1">
    <tr>
        <td>ID</td>
        <td>Nome</td>
        <td>E-mail</td>
        <td>Telefone</td>
        <td>Bairro</td>
        <td>Cidade</td>
        <td>CEP</td>
        <td>Comandos</td>
    </tr>
    <c:forEach var="cliente" items="${lista}">
        <tr>
            <td>${cliente.id}</td>
            <td>
                <a href="cliente?action=ver&id=${cliente.id}">${cliente.nome}</a>
            </td>
            <td>${cliente.email}</td>
            <td>${cliente.tel1}</td>
            <td>${cliente.bairro}</td>
            <td>${cliente.cidade}</td>
            <td>${cliente.cep}</td>
            <td>
                <a href="cliente?action=editar&id=${cliente.id}">Editar</a>
                <a href="cliente?action=deletar&id=${cliente.id}">Excluir</a>
            </td>
        </tr>
    </c:forEach>
</table>

<c:if test="${fn:length(lista) > 0}">
    Existem ${fn:length(lista)} registros.
</c:if><br>

<a href="cliente?action=criar">Adicionar</a>

<%@include file="../template/footer.jsp" %>