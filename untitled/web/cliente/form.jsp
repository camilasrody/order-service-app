<%@include file="../template/header.jsp" %>
<%@ page contentType="text/html;charset=ISO-8859-1" import="java.util.Date, java.text.*" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--${action == 'criar' ? '/cliente?action=criar' : '/cliente?action=editar&id=${cliente.getId()}'}--%>

<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css"/>

<style>.bootstrap-iso .formden_header h2, .bootstrap-iso .formden_header p, .bootstrap-iso form {
    font-family: Arial, Helvetica, sans-serif;
    color: black
}

.bootstrap-iso form button, .bootstrap-iso form button:hover {
    color: white !important;
}

.asteriskField {
    color: red;
}</style>

<c:if test="${erro != null}">

    <p>${erro}</p>

</c:if>

<div class="bootstrap-iso">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <form method="post" action="<c:choose>
                                                <c:when test="${action == 'criar'}">
                                                    /cliente?action=criar
                                                </c:when>
                                                <c:otherwise>
                                                    /cliente?action=editar&id=${cliente.getId()}
                                                </c:otherwise>
                                            </c:choose>">
                    <div class="form-group">
                        <label class="control-label requiredField" for="nome">
                            Nome
                            <span class="asteriskField">
       </span>
                        </label>
                        <input class="form-control" maxlength="30" id="nome" name="nome" value="${cliente.getNome()}"
                               placeholder="Nome Completo" type="text"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label requiredField" for="email">
                            Email
                            <span class="asteriskField">
        *
       </span>
                        </label>
                        <input class="form-control" maxlength="30" id="email" name="email" value="${cliente.getEmail()}"
                               placeholder="seuemail@dominio.com" type="text"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label requiredField" for="tel1">
                            Telefone fixo
                            <span class="asteriskField">
        *
       </span>
                        </label>
                        <input class="form-control" maxlength="10" id="tel1" name="tel1" value="${cliente.getTel1()}"
                               placeholder="(xx) xxxx-xxxx" type="text"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label " for="tel2">
                            Telefone m�vel
                        </label>
                        <input class="form-control" maxlength="10" id="tel2" name="tel2" value="${cliente.getTel2()}"
                               placeholder="(xx) x xxxx-xxxx" type="text"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label requiredField" for="rua">
                            Rua
                            <span class="asteriskField">
        *
       </span>
                        </label>
                        <input class="form-control" maxlength="30" id="rua" name="rua" value="${cliente.getRua()}"
                               placeholder="R. nome da rua" type="text"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label requiredField" for="bairro">
                            Bairro
                            <span class="asteriskField">
        *
       </span>
                        </label>
                        <input class="form-control" maxlength="30" id="bairro" name="bairro"
                               value="${cliente.getBairro()}" placeholder="nome do bairro" type="text"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label " for="cidade">
                            Cidade
                        </label>
                        <input class="form-control" maxlength="30" id="cidade" name="cidade"
                               value="${cliente.getCidade()}" placeholder="Cidade onde mora" type="text"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label " for="cep">
                            CEP
                        </label>
                        <input class="form-control" maxlength="10" id="cep" name="cep" value="${cliente.getCep()}"
                               placeholder="xxxxx-xxx" type="text"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label " for="complemento">
                            Complemento
                        </label>
                        <input class="form-control" maxlength="30" id="complemento" name="complemento"
                               value="${cliente.getComplemento()}" placeholder="casa/apto./bloco" type="text"/>
                    </div>
                    <div class="form-group">
                        <div>
                            <button class="btn btn-primary " name="submit" type="submit">
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<%@include file="../template/footer.jsp" %>